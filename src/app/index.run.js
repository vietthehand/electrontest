(function() {
  'use strict';

  angular
    .module('pajamaPants')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
