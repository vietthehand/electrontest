/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('pajamaPants')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
